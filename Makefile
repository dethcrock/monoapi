.SILENT:

mod:
	go mod tidy

lint:
	golangci-lint run -c golangci-lint.yml