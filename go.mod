module gitlab.com/dethcrock/monoapi

go 1.16

require (
	github.com/hashicorp/go-retryablehttp v0.7.0
	github.com/joho/godotenv v1.4.0
	github.com/json-iterator/go v1.1.12
	go.uber.org/zap v1.20.0
)
