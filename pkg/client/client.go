package client

import (
	"net/http"

	"github.com/hashicorp/go-retryablehttp"
	json "github.com/json-iterator/go"
	"gitlab.com/dethcrock/monoapi/pkg/internal/errs"
	"gitlab.com/dethcrock/monoapi/pkg/internal/types"
	"go.uber.org/zap"
)

type (
	// MonoClient - define struct for monobank client.
	MonoClient struct {
		token  string
		client *retryablehttp.Client
		logger *zap.SugaredLogger
	}
)

// NewClient constructor
func NewClient(opts ...clientOption) *MonoClient {
	client := &MonoClient{
		client: retryablehttp.NewClient(),
		logger: zap.L().Sugar(),
	}

	for _, opt := range opts {
		opt(client)
	}

	return client
}

// GetCurrency - method which return slice of currency.
func (c MonoClient) GetCurrency() ([]types.Currency, error) {
	req, err := retryablehttp.NewRequest(http.MethodGet, currencyURL, nil)
	if err != nil {
		c.logger.Error(err)
		return nil, err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		c.logger.Error(err)
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		clientError := new(types.ClientError)
		if err = json.NewDecoder(resp.Body).Decode(clientError); err != nil {
			return nil, err
		}

		c.logger.Error(clientError)

		return nil, clientError
	}

	defer resp.Body.Close()

	var result []types.Currency

	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, err
	}

	return result, nil
}

// GetInfo - define method which return account data.
func (c MonoClient) GetInfo() (types.Account, error) {
	if c.token == "" {
		return types.Account{}, errs.ErrTokenRequired
	}

	req, err := retryablehttp.NewRequest(http.MethodGet, personalURL, nil)
	if err != nil {
		c.logger.Error(err)

		return types.Account{}, err
	}

	req.Header.Add(signKey, c.token)

	resp, err := c.client.Do(req)
	if err != nil {
		c.logger.Error(err)
		return types.Account{}, err
	}

	if resp.StatusCode != http.StatusOK {
		clientError := new(types.ClientError)
		if err = json.NewDecoder(req.Body).Decode(clientError); err != nil {
			c.logger.Error(err)
		}

		return types.Account{}, clientError
	}

	defer resp.Body.Close()

	var result types.Account
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		c.logger.Error(err)
		return types.Account{}, err
	}

	return result, nil
}
