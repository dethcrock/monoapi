package client

type clientOption func(client *MonoClient)

// WithToken - client option which setup api token for personal client requests.
func WithToken(token string) clientOption {
	return func(client *MonoClient) {
		client.token = token
	}
}
