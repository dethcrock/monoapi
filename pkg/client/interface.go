package client

import "gitlab.com/dethcrock/monoapi/pkg/internal/types"

type (
	// Client - define client interface.
	Client interface {
		Banking
		Account
	}

	// Banking - define banking interface.
	Banking interface {
		GetCurrency() ([]types.Currency, error)
	}

	// Account - define account interface.
	Account interface {
		GetInfo() (types.Account, error)
	}
)
