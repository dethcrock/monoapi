package client

var (
	baseURL = "https://api.monobank.ua/"

	currencyURL = baseURL + "bank/currency"
	personalURL = baseURL + "personal/client-info"
)

var signKey = "X-Token"
