package errs

import "errors"

// Block which define custom errors.
var (
	ErrTokenRequired = errors.New("token required")
)
