package types

// nolint:tagliatelle // provided API rules
type (
	// Card - define monobank card data.
	Card struct {
		Id           string  `json:"id"`
		Balance      float64 `json:"balance"`
		Limit        float64 `json:"creditLimit"`
		CurrencyCode int     `json:"currencyCode"`
		CashbackType string  `json:"cashbackType"`
	}

	// Account - define monobank account data.
	Account struct {
		Name  string `json:"name"`
		Cards []Card `json:"accounts"`
	}
)
