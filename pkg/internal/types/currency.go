package types

import "time"

// nolint:tagliatelle // provided API rules
type (
	// Currency define struct which describe currency data.
	Currency struct {
		CurrencyCodeA int `json:"currencyCodeA"`
		CurrencyCodeB int `json:"currencyCodeB"`
		// Date          Date    `json:"date"`
		Date      int64   `json:"date"`
		RateSell  float32 `json:"rateSell"`
		RateBuy   float32 `json:"rateBuy"`
		RateCross float32 `json:"rateCross"`
	}

	// Date type for custom marshaling
	Date struct {
		time.Time
	}
)
