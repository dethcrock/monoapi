package types

// nolint:tagliatelle // provided API rules
type (
	// ClientError - struct for parsing errors from API.
	ClientError struct {
		Description string `json:"errorDescription"`
	}
)

// Error - implementation for error interface.
func (e ClientError) Error() string {
	return e.Description
}
