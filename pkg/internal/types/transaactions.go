package types

// nolint:tagliatelle // provided API rules
type (
	// Transaction struct describe bank transaction type
	Transaction struct {
		Id              string  `json:"id"`
		Time            Date    `json:"time"`
		Description     string  `json:"description"`
		Mcc             int     `json:"mcc"`
		Hold            bool    `json:"hold"`
		Amount          float64 `json:"amount"`
		OperationAmount float64 `json:"operationAmount"`
		CurrencyCode    int     `json:"currencyCode"`
		CommisionRate   int     `json:"commisionRate"`
		CashbackAmount  float64 `json:"cashbackAmount"`
		Balance         float64 `json:"balance"`
	}
)
