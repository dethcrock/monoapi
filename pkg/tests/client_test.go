package tests

import (
	"os"
	"testing"

	"github.com/joho/godotenv"
	"gitlab.com/dethcrock/monoapi/pkg/client"
)

func Test_GetCurrency(t *testing.T) {
	cl := client.NewClient()

	resp, err := cl.GetCurrency()
	if err != nil {
		t.Fatal(err)
	}

	t.Log(resp)
}

func Test_GetInfo(t *testing.T) {
	if err := godotenv.Load("../../.env"); err != nil {
		t.Fatal(err)
	}

	token, ok := os.LookupEnv("TOKEN")
	if !ok {
		t.Fatal("can`t get TOKEN")
	}

	cl := client.NewClient(client.WithToken(token))

	resp, err := cl.GetInfo()
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("%#v", resp)
}
